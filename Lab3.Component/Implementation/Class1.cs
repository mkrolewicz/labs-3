﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public interface IJakaKawa 
    {
        void Pokaz();
        void Wybierz();
    }

    public interface ITemperatura
    {
        void test1();
        void Dostosuj();
    }

    public interface IIloscCukru 
    {
        void test2();
        int ilosc(int Ilosc);
    }
    public class Kontroler : IJakaKawa, ITemperatura, IIloscCukru
    {
        public void Pokaz()
        {
            Console.WriteLine();
        }

        public void Wybierz()
        {
            Console.WriteLine();
        }

        public void Dostosuj()
        {
            Console.WriteLine();
        }
        public void test1() { Console.WriteLine(); }

        public void test2() { Console.WriteLine(); }

        public int ilosc(int Lyzki)
        {
            return Lyzki;
        }

        //private JakaKawa Kawa;
        //private Temperatura temperatura;
        //private IloscCukru cukier;

        public Kontroler() { }
        //{
        //    //this.Kawa = new JakaKawa();
        //    //this.temperatura = new Temperatura();
        //    //this.cukier = new IloscCukru();
        //}

        //public void Zaparz()
        //{
        //    Kawa.Wybierz();
        //    temperatura.Dostosuj();
        //    cukier.ilosc(4);
        //}

    }

    //class JakaKawa : IJakaKawa
    //{

    //    public void Pokaz()
    //    {
    //        Console.WriteLine("test");
 
    //    }

    //    public void Wybierz()
    //    {
    //        Console.WriteLine("test");
 
    //    }
    //}
    //class Temperatura : ITemperatura
    //{
    //    public void Dostosuj()
    //    {
 
    //    }
    //    public void test1(){}
    //}
    //class IloscCukru : IIloscCukru
    //{
    //    public void test2(){}
    //    public int ilosc(int Lyzki)
    //    {
    //        return Lyzki;
    //    }
    //}
}
